#!/usr/bin/env python

from os import listdir, path
import opc, time, tarfile

numLEDs = 256 # 64x4
neo = opc.Client('localhost:7890')

off = [ (0,0,0) ] * numLEDs
splashScreen = [ (0,0,0) ] * numLEDs
splashScreen[numLEDs/2] = (64,0,0)

print "load images"
imagesList = []
dirName = "/home/pi/diaporama/data"

# archive = "/home/pi/diaporama/data/icon.tar"
# tar = tarfile.open(archive)
# for i, member in enumerate(tar.getmembers()):
	# f = tar.extractfile(member)
	# data = eval(f.read())

	# imagesList.append(data)
	# if i % 50  == 0:
		# neo.put_pixels(splashScreen)
		# time.sleep(0.1)
		# neo.put_pixels(off)
		# time.sleep(0.1)

for i, file in enumerate(sorted(listdir(dirName))):
	if file.lower().endswith(".dat"):
		filepath = path.join(dirName, file)
		with open(filepath,"r") as f:
			data = eval(f.read())

		imagesList.append(data)
		if i % 50  == 0:
			neo.put_pixels(splashScreen)
			time.sleep(0.1)
			neo.put_pixels(off)
			time.sleep(0.1)

print "start show"
neo.put_pixels(off)
time.sleep(1)
while True:
	try:
		for data in imagesList:
			neo.put_pixels(data)
			time.sleep(0.6)
			neo.put_pixels(off)
			time.sleep(0.1)

	except KeyboardInterrupt:
		neo.put_pixels(off)
		neo.disconnect()
		print "exit"
		exit()


