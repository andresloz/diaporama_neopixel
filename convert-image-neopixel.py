#! /usr/bin/env python
#-*- coding: utf-8 -*-
import sys, os
from PIL import Image

NAME = "Image Neopixels"
VERSION = "0.0.1"
DATE = "2019 November 3"
AUTHOR = "Andres Lozano a.k.a Loz"
COPYRIGHT = "Copyleft: This is a free work, you can copy, distribute, and modify it under the terms of the Free Art License http://artlibre.org/licence/lal/en/"
URL = "http://hyperficiel.com"
DESCRIPTION = "convert image to mini Neopixels Screen standard"

class ImageNeopixels:
	"""Image to Neopixels screen: example with 4 x 64 pixels screen
	Every pixel is an tuple (R,G,B)
	|		screen 		0		|		|		screen		1		|
	0	1	2	3	4	5	6	7		8	9	10	11	12	13	14	15
	-	-	-	-	-	-	-	-		-	-	-	-	-	-	-	-
	0	x	x	x	x	x	x	x		x	x	x	x	x	x	x	x
	1	x	x	x	x	x	x	x		x	x	x	x	x	x	x	x
	2	x	x	x	x	x	x	x		x	x	x	x	x	x	x	x
	3	x	x	x	x	x	x	x		x	x	x	x	x	x	x	x
	4	x	x	x	x	x	x	x		x	x	x	x	x	x	x	x
	5	x	x	x	x	x	x	x		x	x	x	x	x	x	x	x
	6	x	x	x	x	x	x	x		x	x	x	x	x	x	x	x
	7	x	x	x	x	x	x	x		x	x	x	x	x	x	x	x
	
	|		screen 		2		|		|		screen		3		|
	0	1	2	3	4	5	6	7		0	1	2	3	4	5	6	7
	-	-	-	-	-	-	-	-		-	-	-	-	-	-	-	-
	8	x	x	x	x	x	x	x		x	x	x	x	x	x	x	x
	9	x	x	x	x	x	x	x		x	x	x	x	x	x	x	x
	10	x	x	x	x	x	x	x		x	x	x	x	x	x	x	x
	11	x	x	x	x	x	x	x		x	x	x	x	x	x	x	x
	12	x	x	x	x	x	x	x		x	x	x	x	x	x	x	x
	13	x	x	x	x	x	x	x		x	x	x	x	x	x	x	x
	14	x	x	x	x	x	x	x		x	x	x	x	x	x	x	x
	15	x	x	x	x	x	x	x		x	x	x	x	x	x	x	x

	"""
	def __init__(self, file=None, params={"size":(16,16), "export":"int-lists", "rotate":0, "posterize":3}):	
		for k,v in params.iteritems():
			setattr(self,k,v)
			
		image = Image.open(file)
		self.imageSource = image.resize(self.size)
		
		
	def posterizeValues(self, rgb_values=[(0, 0, 0)], shift=3):
		# to prevent excess led power consumption
		# posterize byte using shift operator ex: posterize val = 256 >> 3
		new_values = [tuple([x >> shift for x in color]) for color in rgb_values]
		return new_values
		
	def pixelsToNeopixels(self, pixels=None):
		width, height = self.size
		rgbList = ["none"]*(width*height)
		wMatrix = (width / 8)
		hMatrix = (height / 8)
		index = 0
		
		for j2 in xrange(wMatrix):
			for i2 in xrange(hMatrix):
				for j in xrange(j2*8,(j2*8)+8):
					for i in xrange(i2*8,(i2*8)+8):
						rgbList[index] = pixels[(j*width)+i]
						index += 1
				
		return rgbList
		
	def getNeopixels(self): # any type of image and size
		data = list(self.imageSource.getdata())
		data = self.posterizeValues(rgb_values=data, shift=self.posterize)
		dataArray = self.pixelsToNeopixels(data)
		return dataArray
		
if __name__ == "__main__":
	print NAME,VERSION
	print "\n".join([DATE,AUTHOR,COPYRIGHT,URL])
	
	if len(sys.argv) > 1:
		file = sys.argv[1]
		dir = os.path.dirname(file)
		base, ext = os.path.splitext(sys.path.basename(file))
	else:
		file = "minilisa.png"
		dir = os.path.dirname(os.path.realpath(__file__))
		base = "minilisa"
		
	o = ImageNeopixels(file=file)
	data = o.getNeopixels()
	del o
	
	print dir
	# convert image to neopixels data
	file = os.path.join(dir, base+".dat")
	with open(file,"w") as f:
		f.write(repr(data))

